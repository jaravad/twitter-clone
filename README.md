# Assignment-01

Everything was coded using plain HTML & CSS at first and then React was used. I plan to develop the rest of the project using React as UI Framework. By the moment everything is being displayed in a single component.

One of the Frontend related objectives for the following assignments is to componentize the project as much as possible in order to heavily reuse the code, avoid code repetition and handle UI interaction in a better way.

The project is live running at: [http://my-twitter.surge.sh/](http://my-twitter.surge.sh/).

## How to run the App

Run the following commands in the project directory.

1. Install the project dependencies:

<pre><code>npm install</code></pre>

2. Start the development server at [http://localhost:3000](http://localhost:3000) by running:

<pre><code>npm start</code></pre>

### About styles and assets

One media query was used to modify the styles properly to achieve a responsive behavior in mobile viewports.

In addition to replicating the provided UI design some subtle effects were implemented in this assignment, such as hover effects for buttons and focus effects for inputs. These effects are based on the ones from Twitter's UI.

SASS was used as CSS preprocessor, some SASS color functions, such as **transparentize()** and **scale-color()**  were used to implement the effects mentioned above. The followind SASS features were used to better structure styles:

- **SASS variables** for colors and fonts stack.
- **SASS partials** to separate styles in multiple files. There is one file that contains a set of simple CSS rules to get rid of some default CSS styles applied by the brwoser, and there is another file for importing some web fonts.
- **SASS @import** to import the two files mentioned in the previous bullet entry into a main SCSS file that contains all the styles for the main UI.
- **SASS nesting** to handle CSS specificity.
- **SASS Reference Symbol** to easily implement hover and focus effects.

**Inter UI** font was used for the project, this is a free for commercial use font used as alternative font since Helvetica Neue (Twitter's official font) is not a free to use font. The font is self hosted and the font assets were generated using a [Web Font Generator](https://www.fontsquirrel.com/tools/webfont-generator).

### Deployment

Although this was not required in the assignment, a CDN service called [Surge](https://surge.sh/) was used to deploy the project.

## Sources for assets:

- [Twitter Brand Guidelines](https://about.twitter.com/content/dam/about-twitter/company/brand-resources/en_us/Twitter_Brand_Guidelines_V2_0.pdf)
- [Twitter Brand Resources](https://about.twitter.com/en_us/company/brand-resources.html)
- [Inter Font by Rasmus Andersson](https://www.fontsquirrel.com/fonts/inter)
- [Web Font Generator](https://www.fontsquirrel.com/tools/webfont-generator)

## Sources for deployment:

- [Surge](https://surge.sh/)
