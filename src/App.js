import React from 'react';
import logo from './assets/images/logo.svg';

const App = () => {
  return (
    <div className="login__page">
      <header className="login__header">
        <div className="input__container self-start">
          <label for="username">
            <input type="text" id="username" />
            <span>Phone, email, or username</span>
          </label>
        </div>

        <div className="input__container">
          <label for="password">
            <input type="password" id="password" />
            <span>Password</span>
          </label>
          <a href="/">Forgot password?</a>
        </div>

        <a href="/" className="btn btn--unfilled" role="button">
          Log in
        </a>
      </header>

      <main className="main__signin">
        <img src={logo} alt="Twitter logo" width="35" />

        <h1>See what's happening in the world right now</h1>

        <strong>Join Twitter today.</strong>

        <a className="btn btn--filled d-block" href="/" role="button">
          Sign up
        </a>

        <a className="btn btn--unfilled d-block" href="/" role="button">
          Log in
        </a>
      </main>

      <footer>
        <nav>
          <ul>
            <li>
              <a href="/">About</a>
            </li>
            <li>
              <a href="/">Help Center</a>
            </li>
            <li>
              <a href="/">Terms</a>
            </li>
            <li>
              <a href="/">Privacy Policy</a>
            </li>
            <li>
              <a href="/">Cookies</a>
            </li>
            <li>
              <a href="/">Ads info</a>
            </li>
            <li>
              <a href="/">Blog</a>
            </li>
            <li>
              <a href="/">Status</a>
            </li>
            <li>
              <a href="/">Jobs</a>
            </li>
            <li>
              <a href="/">Brand</a>
            </li>
            <li>
              <a href="/">Advertise</a>
            </li>
            <li>
              <a href="/">Marketing</a>
            </li>
            <li>
              <a href="/">Bussinesses</a>
            </li>
            <li>
              <a href="/">Developers</a>
            </li>
            <li>
              <a href="/">Directory</a>
            </li>
            <li>
              <a href="/">Settings</a>
            </li>
            <li>© 2020 Twitter, Inc.</li>
          </ul>
        </nav>
      </footer>
    </div>
  );
};

export default App;
